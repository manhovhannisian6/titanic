import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    mr_info = [0, []]
    miss_info = [0, []]
    mrs_info = [0, []]

    def a(data, row):
        if row['Age']:
            data[1].append(row['Age'])
        else:
            data[0] += 1

    for i in range(df.shape[0]):
        row = df.iloc[i]
        if 'Mr.' in row['Name']:
            a(mr_info, row)
        if 'Mrs.' in row['Name']:
            a(mrs_info, row)
        if 'Miss.' in row['Name']:
            a(miss_info, row)

    return [('Mr.', mr_info[0], sum(mr_info[1]) / len(mr_info[1])), ('Mrs.', mr_info[0], sum(mrs_info[1]) / len(mrs_info[1])), ('Miss.', mr_info[0], sum(miss_info[1]) / len(miss_info[1]))]
